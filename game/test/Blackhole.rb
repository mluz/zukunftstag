class Blackhole
	def initialize(game_window)
		@game_window = game_window
    @icon = Gosu::Image.new(@game_window, "gosu/blackhole.png", true)
    @x = 20
		@y = 30
    @radius = 100
	end
  
  def swallow(players, balls)
    players.any? {|player|Gosu::distance(@x, @y, player.x, player.y) < @radius}
    balls.any?{|ball|Gosu::distance(@x, @y, ball.x, ball.y) < @radius}
  
  end
  def draw
	  @icon.draw(@x,@y, 1)
	end
  
 
end
