require_relative 'GosuGameImg'
require_relative 'Body'
require_relative 'BlackholeImplicit'
require_relative 'Bullet'
require_relative 'Explosion'
require_relative 'PlayerImplicit'
require_relative 'BallImplicit'

def alpha_to_color(alpha)
    Gosu::Color.new((alpha * 0xff).to_i, 255, 255, 255)
end

def rand_around_0
	2 * rand - 1.0 
end

