require_relative 'SpaceGame'
module SpaceGame
class Body	< GosuGameImg
	INITIAL_LIFE = 3.0
	def initialize(game_window, x, y, life, icon_file, coupling)
		super(game_window, x, y, icon_file)
		@alpha = 1.0
		@initial_life = 3.0
		@life = life = 3.0
		@coupling = coupling
	end
	
	def life
		@life
	end
	def coupling
		@coupling
	end
	
	def update_position_x(delta)
		@x = @x + delta
	end
	
	def update_position_y(delta)
		@y = @y + delta
	end
	
	def gain_life
		@life = @life + 1
		if (@life < @initial_life)
			@alpha = @alpha + 1.0/@initial_life
		end
	end
  
  def reduce_life
	@life = @life - 1.0
	if(@life <@initial_life)
		@alpha = @alpha - 1.0/@initial_life
	end
	if (@life <= 0) 
		@game_window.stop_game
		puts "game Stopped"
	end
  end
	
	def draw(prio)
		if(@life > 0)
			@icon.draw(@x, @y, prio, factor_x = 1, factor_y = 1, alpha_to_color(@alpha))
		end
	end
	
	def vanish
		@life = -1
		reset_position
	end
	
	def reset_position
		 @override
	end
	
	def reset
		reset_position
		@life = @initial_life
	end
	
end
end