require_relative 'SpaceGame'
module SpaceGame
class Ball < Body
def initialize(game_window)
		xvalue = rand(game_window.width)
		super(game_window, rand(game_window.width), 0, INITIAL_LIFE, "img/asteroid.png", 120.0)
		@speed = (1 + rand(15))/2.0
		@radius = 30
		@initial_life = 1.0
		@value = 1.0
	end
	  
	  def getRandomWith
		rand(@game_window.width)
	  end
	  
	  def update(players)
	   if @y > @game_window.height
		reset
	   end
		@y = @y + @speed
		hits(players)
	  end
	  def getValue
		@value
	end
	
  def reset_position
    @y = 0
    @x = getRandomWith
    @speed = (1 + rand(15))/2.0
  end
end
end
