#TODO For some reason it expects player to be an innerclass of MyGame
#try to fix this

require_relative 'SpaceGame'
module SpaceGame
class Player < SpaceGame::Body  
  STEP = 10.0


  def initialize(game_window, x, y, icon_name, name)
	super(game_window,x, y,INITIAL_LIFE, icon_name, 15.0) 
	@startX = x
	@startY = y
	@name = name
	@initial_life = 3.0
	@points = 0.0
  end
  
  
  def reset_position
	@x = @startX
	@y = @startY
  end
  
  def earnPoints(amount)
	@points += amount
  end
 
  
  def shoot
	@game_window.bullets << Bullet.new(@game_window, center_x, @y, self)
  end
  
  def vanish
	@life = -1
	@game_window.stop_game
  end
end
end

