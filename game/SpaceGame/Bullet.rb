require_relative 'SpaceGame'
module SpaceGame
class Bullet < Body  

  def initialize(game_window, x, y, player)
    super(game_window, x, y, INITIAL_LIFE, "img/laser.png", 600.0)
    @player = player
    @angle = 0
	@initial_life = 1.0
    @speed = 20
    @alpha = 0.5
  end

  def update 
    @y -= @speed       
    collide = false    
	target = @game_window.balls
	ballsHit = target.find_all{|ball| distance(ball) < 50}
	if not ballsHit.empty?
		ballsHit.each{|ball| ball.explode}
		points = ballsHit.reduce(0) do |sum, ball|
			sum + ball.getValue
		end
		@player.earnPoints(points)
		collide = true
	end
    # destroy the bullet when it reaches the top of the screen or collides with an asteroid
    if @y <= -@icon.height / 2 or collide
	   die
	end
  end
  
  def draw
      @icon.draw_rot(@x, @y,1, @angle, 0.5, 0.5, 1.0, 1.0, alpha_to_color(@alpha))
  end
  
  def dead
	@life <= 0
  end
  
  def die	
	@life = 0
  end
end
end