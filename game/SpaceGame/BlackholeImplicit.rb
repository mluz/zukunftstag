require_relative 'GosuGameImg'
module SpaceGame
class Blackhole < GosuGameImg
def initialize(game_window)		
		super(game_window, rand(game_window.width-200) + 100, rand(game_window.height-200) + 100, "img/blackhole.png")
		@radius = 50
		@speedx =  rand_around_0 * 0.1
		@speedy =  rand_around_0 * 0.1
		
		    
	end
	
	def drift
		@x = @x  + @speedx
		@y = @y + @speedy
	end
	
	
	def force(body)
		dist = body.coupling/(distance(body) * distance(body))
		force = {:x => (center_x - body.center_x) * dist, :y => (center_y - body.center_y) * dist}
	end 
 
 
 def update
	#let the blackhole itself float around
	#drift
	drag(@game_window.players)
	drag(@game_window.balls)
	drag(@game_window.bullets)
	
	swallow(@game_window.players)
	swallow(@game_window.balls)
 end
 
  def drag(bodies)
   bodies.each do |body| 
		f = force(body)
		body.update_position_x(f[:x])
		body.update_position_y(f[:y])
	end
  end
  
	def draw
	  @icon.draw(@x,@y, 1)
	end
  
	def reset
		reset_position
	end
	
	def reset_position
		@x = rand(100..@game_window.width) - 100
		@y = rand(100..@game_window.height) - 100
	end

end
end