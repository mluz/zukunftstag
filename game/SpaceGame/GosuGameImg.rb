require_relative 'SpaceGame'
module SpaceGame
class GosuGameImg
	def initialize(game_window, x, y, icon_file)
		@game_window = game_window
		@icon = Gosu::Image.new(game_window, icon_file, true)
		@x = x 
		@y = y
	end
	
	def x
		@x
	end
	
	def y
		@y
	end
		
	def distance(body)
		Gosu::distance(center_x,center_y, body.center_x, body.center_y)
	end
		
	def center_x	
		@x + @icon.width/2
	end
	def center_y
		puts 
		@y + @icon.height/2
	end
end
end