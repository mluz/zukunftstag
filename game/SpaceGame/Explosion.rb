require_relative 'SpaceGame'
module SpaceGame
class Explosion
	DELTA_A = 1.0/30
	DELTA_R = 1.0/30
	DELTA_W = 12.0

	def initialize(game_window, x, y)
			@game_window = game_window
			@icon = Gosu::Image.new(game_window, "img/boom.png", true)
			@alpha = 1.0
			@scaling = (rand(2) + 1) * 2
			
			@dead = false
			@x = x
			@y = y
	end
	
	def update
		@scaling += DELTA_R
		@alpha -= DELTA_A 
		@dead = true  if @alpha <= 0
	end
	
	def draw
		@icon.draw_rot(@x, @y, 1, 12.0, center_x = 0.5, center_y = 0.5,  factor_x = @scaling, factor_y = 1.6, alpha_to_color(@alpha)) if not @dead
	end
end
end