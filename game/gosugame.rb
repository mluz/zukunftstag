require 'gosu'
require_relative 'SpaceGame/SpaceGame'
require_relative 'Player' 
require_relative 'Ball'
require_relative 'Blackhole'

class MyGame < Gosu::Window
  def initialize
    @width = 1200
    @height = 800
    @running = true
	@resume = true

    super(@width,@height,false)
    @sign = Gosu::Image.new(self, "img/sign_post.png", false)
    
	@font_color = Gosu::Color.argb(0x99cc0000)
	@game_state = Gosu::Font.new(self, "Arial", 24)
    @player1 = SpaceGame::Player.new(self, 0 , @height-90, "img/player1.png", "Apollo")
	@player2 = SpaceGame::Player.new(self, @width-100, @height-100, "img/marvin.png", "Marvin")
	@players = [@player1, @player2]
	@bullets = []
	@explosions = []
    @balls = 3.times.map{SpaceGame::Ball.new(self)}  
    @blackholes = 2.times.map{SpaceGame::Blackhole.new(self)} 
  end

  def update
    if @running
		#move around the player 1
		if button_down? Gosu::Button::KbLeft
			@player1.move_left
		end
		if button_down? Gosu::Button::KbRight
			@player1.move_right
		end
		if button_down? Gosu::Button::KbDown
			@player1.move_down
		end
		if button_down? Gosu::Button::KbUp
			@player1.move_up
		end
		#move around player 2
		if button_down? Gosu::Button::KbA
			@player2.move_left
		end
		if button_down? Gosu::Button::KbD
			@player2.move_right
		end
		if button_down? Gosu::Button::KbS
			@player2.move_down
		end
		if button_down? Gosu::Button::KbW
			@player2.move_up
		end
		if button_down? Gosu::Button::KbSpace
			@player1.shoot
		end
		#update game
		@balls.each{|ball|ball.update(@players)}
		@bullets.each{|bullet| bullet.update}
		@explosions.each{|explosion|explosion.update}
		@blackholes.each{|bh| bh.update}
		
    else
      if (button_down? Gosu::Button::KbEscape) 
        restart_game
      end
      
    end
  end
  
  def balls
	@balls
  end
  def bullets
	@bullets
  end
  def explosions
	@explosions
  end
  def players
	@players
  end
  
  def forces
	@blackholes
  end

  def draw
    @players.each{|player| player.draw(3)}
    @balls.each{|ball|ball.draw(5)}
	@blackholes.each{|blackhole| blackhole.draw}
	@bullets = @bullets.select{ |bullet| !bullet.dead} 
	@bullets.each{|bullet|bullet.draw}
	
	@explosions.each{|explosion|explosion.draw}
	update_game_state
	draw_game_over
  end
  
  def update_game_state
	signX = @width - @sign.width
	signY = @height - @sign.height
	@sign.draw(signX,signY , 1)
	indent = 36
	lineheight = 20
	textPosX = signX + indent
	textPosY = signY + 2 * lineheight
	#@labels =  Gosu::Font.from_text(self, text, font_name, font_height, line_spacing, width, align) 
	@game_state.draw_rel("life:", textPosX + 68 , textPosY, 2, 0.0, 1.0, factor_x = 1, factor_y = 1, @font_color)
	@game_state.draw_rel("points:", textPosX + 68 + 56, textPosY, 2, 0.0, 1.0, factor_x = 1, factor_y = 1, @font_color)
	@players.each do |player| 
		textPosY += lineheight
		@game_state.draw_rel( player.name + ": ", textPosX, textPosY, 1, 0.0, 1.0, factor_x = 1, factor_y = 1,@font_color)
		@game_state.draw_rel( player.life.to_s, textPosX + 68, textPosY, 1, 0.0, 1.0, factor_x = 1, factor_y = 1,@font_color)
		@game_state.draw_rel( player.points.to_s, textPosX + 68 + 56, textPosY, 1, 0.0, 1.0, factor_x = 1, factor_y = 1,@font_color)
	end
  end
  
  def draw_game_over
		
	
	if(!@resume && !@running)
		winner = @players.sort_by{|player| [-player.life, player.name]}.first
		banner = Gosu::Image.new(self, "img/banner.png", false)
		bannerX = (@width - banner.width)/2.0
		bannerY = (@height - banner.height)/2.0
		banner.draw(bannerX, bannerY, 11)
		font_size = 70
		y_pos = bannerY + 100
		Gosu::Font.new(self, "Arial", font_size).draw_rel("Game Over: ", @width/2, y_pos, 12, 0.5, 0.5, factor_x = 1, factor_y = 1,@font_color)
		Gosu::Font.new(self, "Arial", font_size).draw_rel(winner.name + " wins with " + winner.points.to_s + " points", @width/2, y_pos + font_size, 12, 0.5, 0.5,factor_x = 0.9, factor_y = 1, @font_color, :default) 
	end
  end
  
  def stop_game
	puts " stopp!"
	
	@running = false
	@resume = false
  end
  
  def pause_game
    @running = false
	@resume = true
  end
  
  
  def resume_game
    @running = true
    @balls.each{|ball|ball.reset}
  end
  
  def restart_game
    @running = true
    @resume = true
    @balls.each{|ball|ball.reset}
    @players.each{|p|p.reset}
    @blackholes.each{|bh|bh.reset}
  end
  
  

end


window = MyGame.new
window.show
