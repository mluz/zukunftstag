require_relative 'SpaceGame/SpaceGame'

class SpaceGame::Ball < SpaceGame::Body
	  
	def hits(players)
		playersHit = players.find_all{|player| distance(player) < @radius}
		
		playersHit.each do |player| 
			#puts player.name + "hit by ball" + distance(player).to_s + "radius " + @radius.to_s
			player.reduce_life
			sleep(1)
			player.reset_position
			reset
		end
	
	end
	
	def explode
		@game_window.explosions << SpaceGame::Explosion.new(@game_window, @x + @icon.width/2, @y)
		reset
	end
	
end
