require_relative 'SpaceGame/SpaceGame'

class SpaceGame::Blackhole < SpaceGame::GosuGameImg
  
	def swallow(things)
		bodies_to_eat = things.find_all {|body| distance(body) < @radius}
		bodies_to_eat.each do |body| 
			body.vanish
		end
	end
	
	
end
