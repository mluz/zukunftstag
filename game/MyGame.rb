###main class: zum Programmieren von unserem Spiel

require 'gosu'
require_relative 'SpaceGame/SpaceGame'
require_relative 'Player'
require_relative 'Ball'
require_relative 'Blackhole'

class MyGame < Gosu::Window
  def initialize
    @width = 1200
    @height = 800
    @running = true
	@resume = true

    super(@width,@height,false)
    @sign = Gosu::Image.new(self, "img/sign_post.png", false)
    
	@font_color = Gosu::Color.argb(0x99ccff00)
	@game_state = Gosu::Font.new(self, "Arial", 24)
    @player1 = SpaceGame::Player.new(self, 0 , @height-90, "img/player1.png", "Apollo")
	@player2 = SpaceGame::Player.new(self, @width-100, @height-100, "img/marvin.png", "Marvin")
	@players = [@player1]
	@bullets = []
	@explosions = []
    @balls = 0.times.map{SpaceGame::Ball.new(self)}  
    @blackholes = 0.times.map{SpaceGame::Blackhole.new(self)} 
  end

  def update
    if @running
		#bewege Player1 mit Hilfe der Pfeil Tasten (Schau dir Player.rb and)
		if button_down? Gosu::Button::KbLeft
		end
		if button_down? Gosu::Button::KbRight
		end
		#kannst du ihn auch rauf und runter bewegen? (schau dir Player.rb an...)
		
		#kannst du den zweiten Spieler (s. o. Player2) auf das Feld bringen? 
		
		#was gibt es sonst noch in unserem Spiel? (schau mal oben nach, was sind eigentlich diese Bälle?)
		
		#Wenn der Ball einen Spieler trifft, was passiert dann? wie kannst du trotzdem weiter spielen?
		
		#versuch dich zu wehren gegen diese Bälle, (Schau nochmal Player.rb an)
	
		#und was machen eigentlich diese blackholes? Was heisst das eigentlich und was könnte es sein?
	
		update_all
		
    else
      if (button_down? Gosu::Button::KbEscape) 
        restart_game
      end
      
    end
  end
  
  def balls
	@balls
  end
  def bullets
	@bullets
  end
  def explosions
	@explosions
  end
  def players
	@players
  end
  
  def forces
	@blackholes
  end
  
  

  
  def stop_game
	puts " stopp!"
	
	@running = false
	@resume = false
  end
  
  def pause_game
    @running = false
	@resume = true
  end
  
  
  def resume_game
    @running = true
    @balls.each{|ball|ball.reset}
  end
  
  def restart_game
    @running = true
    @resume = true
    @balls.each{|ball|ball.reset}
    @players.each{|p|p.reset}
    @blackholes.each{|bh|bh.reset}
  end
  
  def draw
    @players.each{|player| player.draw(3)}
    @balls.each{|ball|ball.draw(5)}
	@blackholes.each{|blackhole| blackhole.draw}
	@bullets = @bullets.select{ |bullet| !bullet.dead} 
	@bullets.each{|bullet|bullet.draw}
	
	@explosions.each{|explosion|explosion.draw}
	update_game_state
	draw_game_over
  end
  
  def update_all
  #update game -> move to implicit class...
		@balls.each{|ball|ball.update(@players)}
		@bullets.each{|bullet| bullet.update}
		@explosions.each{|explosion|explosion.update}
		@blackholes.each{|bh| bh.update}
	
  end
  
  def update_game_state
	icon_scaling_factor = 1.3
	signX = @width - @sign.width * icon_scaling_factor
	signY = @height - @sign.height * icon_scaling_factor
	@sign.draw(signX,signY , 1, icon_scaling_factor, icon_scaling_factor)
	indent = 80
	lineheight = 24
	textPosX = signX + 40
	textPosY = signY + 2 * lineheight
	#@labels =  Gosu::Font.from_text(self, text, font_name, font_height, line_spacing, width, align) 
	@game_state.draw_rel("life:", textPosX + indent , textPosY, 2, 0.0, 1.0, factor_x = 1, factor_y = 1, @font_color)
	@game_state.draw_rel("points:", textPosX + 2 * indent, textPosY, 2, 0.0, 1.0, factor_x = 1, factor_y = 1, @font_color)
	@players.each do |player| 
		textPosY += lineheight
		@game_state.draw_rel( player.name + ": ", textPosX, textPosY, 1, 0.0, 1.0, factor_x = 1, factor_y = 1,@font_color)
		@game_state.draw_rel( player.life.to_s, textPosX + indent, textPosY, 1, 0.0, 1.0, factor_x = 1, factor_y = 1,@font_color)
		@game_state.draw_rel( player.points.to_s, textPosX + 2 * indent, textPosY, 1, 0.0, 1.0, factor_x = 1, factor_y = 1,@font_color)
	end
  end
  
  def draw_game_over
	if(!@resume && !@running)
		winner = @players.sort_by{|player| [-player.life, player.name]}.first
		banner = Gosu::Image.new(self, "img/banner.png", false)
		bannerX = (@width - banner.width)/2.0
		bannerY = (@height - banner.height)/2.0
		banner.draw(bannerX, bannerY, 11)
		font_size = 70
		y_pos = bannerY + 100
		Gosu::Font.new(self, "Arial", font_size).draw_rel("Game Over: ", @width/2, y_pos, 12, 0.5, 0.5, factor_x = 1, factor_y = 1,@font_color)
		Gosu::Font.new(self, "Arial", font_size).draw_rel(winner.name + " wins with " + winner.points.to_s + " points", @width/2, y_pos + font_size, 12, 0.5, 0.5,factor_x = 0.9, factor_y = 1, @font_color, :default) 
	end
  end
  

end


