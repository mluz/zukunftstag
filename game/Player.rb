require_relative 'SpaceGame/SpaceGame'

class SpaceGame::Player < SpaceGame::Body

def name
	@name
end
  
  def points
	@points
  end
  
  def shoot
	@game_window.bullets << SpaceGame::Bullet.new(@game_window, center_x, @y, self)
  end
  
  def move_left
    @x = @x - STEP
    if @x < 0
      @x = @game_window.width + @x
    end
  end
  
  def move_right
    @x = @x + STEP
    if @x > @game_window.width 
      @x = @x - @game_window.width
    end
  end
  
  def move_down
    @y = @y + STEP
    if @y > @game_window.height
      @y = 2 * @game_window.height - @y - @icon.height
    end
  end
  def move_up
    @y = @y - STEP
    if @y < 0 
      @y = -2* @y
    end
  end
  
end
