Turtle.draw do
 backgroundcolor beige
 pencolor crimson
 pensize 2
 
 100.times do
  steps = rand(1000)
  forward steps
  backward steps
  turnright(rand(360))
  end
end